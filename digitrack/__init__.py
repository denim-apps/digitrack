import frappe

__version__ = '0.0.1'

@frappe.whitelist(allow_guest=True)
def check_code(code):
  exists = frappe.db.exists('Tracking Chip', { 'scratch_code': code })

  if exists:
    return {
      'found': True,
      'data': frappe.get_value('Tracking Chip', { 'scratch_code': code }, 'chip_data')
    }

  return {
    'found': False
  }
